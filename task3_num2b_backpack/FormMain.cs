﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using task3_num2b_backpack.src;

namespace task3_num2b_backpack
{
    public partial class FormMain : Form
    {
        /* Сравнить решения, полученные точным алгоритмом и генетическим алгоритмом. 
           Сравнить время работы и оптимальность. */

        // 
        private List<Element> _initialSet;
        private Algorithm _algorithm;
        private int _size;
        private int _count;

        public FormMain()
        {
            InitializeComponent();
            dgvSubjects.RowCount = 2;
            dgvSubjects.ColumnCount = (int) numCount.Value;

            int height = dgvSubjects.Height/2;
            int width = dgvSubjects.Width/dgvSubjects.ColumnCount;
            for (int i = 0; i < dgvSubjects.RowCount; ++i)
            {
                dgvSubjects.Rows[i].Height = height;
                ((DataGridViewTextBoxColumn)dgvSubjects.Columns[i]).MaxInputLength = 2;
            }
            for (int i = 0; i < dgvSubjects.ColumnCount; ++i)
            {
                dgvSubjects.Columns[i].Width = width;
                ((DataGridViewTextBoxColumn)dgvSubjects.Columns[i]).MaxInputLength = 2;
            }

            _size = dgvSubjects.ColumnCount * 2;
        }

        private void numCount_ValueChanged(object sender, EventArgs e)
        {
            dgvSubjects.ColumnCount = (int) numCount.Value;

            int width = dgvSubjects.Width/dgvSubjects.ColumnCount;
            for (int i = 0; i < dgvSubjects.ColumnCount; ++i)
                dgvSubjects.Columns[i].Width = width;

            int newSize = dgvSubjects.ColumnCount * 2;
            // если количество предметов уменьшилось, очищаем таблицу для корректной работы
            if (_size > newSize)
            {
                for (int i = 0; i < dgvSubjects.ColumnCount; ++i)
                {
                    dgvSubjects[i, 0].Value = string.Empty;
                    dgvSubjects[i, 1].Value = string.Empty;
                }
                _count = 0;
            }
            _size = newSize;
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            if (_count < _size)
                MessageBox.Show(@"Заполните таблицу полностью!");
            else
            {
                _initialSet = new List<Element>();

                for (int i = 0; i < dgvSubjects.ColumnCount; ++i)
                    _initialSet.Add(new Element(int.Parse(dgvSubjects[i, 0].Value.ToString()),   // weight
                                                int.Parse(dgvSubjects[i, 1].Value.ToString()))); // price

                _algorithm = new Algorithm((int) numWeight.Value, _initialSet);

                int[] res;
                _algorithm.GetSolve(out res);
                tbEnumerate.Clear();
                tbEnumerate.AppendText("В рюкзак положили предметы: " + "\r\n" + ToString(res) + "\r\n");

                _algorithm.GetGeneticSolve(out res);
                tbGenetic.Clear();
                tbGenetic.AppendText("В рюкзак положили предметы: " + "\r\n" + ToStringGenetic(res));
            }
        }

        private void dgvSubjects_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int col = e.ColumnIndex, row = e.RowIndex;
            if (dgvSubjects[col, row].Value != null)
                _count++;
            else
                _count--;
        }

        private static string ToString(IEnumerable<int> list)
        {
            string res = "";
            int i = 0;
            foreach (var item in list)
            {
                res += i + "-й тип: " + item + " штук" + "\r\n";
                i++;
            }
            return res;
        }

        private static string ToStringGenetic(IEnumerable<int> list)
        {
            string res = "";
            int i = 0;
            foreach (var item in list)
            {
                res += i + "-й тип: ";
                if (item > 0)
                    res += " да" + "\r\n";
                else
                    res += " нет" + "\r\n";
                i++;
            }
            return res;
        }

        #region Validation

        // функция проверки символа на корректность: допустимы только цифры
        private static bool CheckKey(KeyPressEventArgs e)
        {
            return e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)Keys.Delete ||
                   e.KeyChar == (char)Keys.Back;
        }

        private static void KeyPressed(object sender, KeyPressEventArgs e)
        {
            e.Handled = !CheckKey(e);
        }

        private void dgvSubjects_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += KeyPressed;
        }

        #endregion
    }
}
