﻿using System.Collections.Generic;
using System.Linq;

namespace task3_num2b_backpack.src.Genetic
{
    internal class Backpack
    {
        private static Element[] _elements;
        public static uint Size { get; private set; }
        private static double _totalPrice;
        public static double MaxWeight { get; private set; }

        public Backpack() { }

        public Backpack(List<Element> elements, double maxWeight)
        {
            _elements = elements.ToArray();
            MaxWeight = maxWeight;
            Size = (uint)_elements.Length;
            _totalPrice = _elements.Sum(e => e.Price);
        }

        public int GetPrice(Genom genom)
        {
            int price = 0;
            for (uint i = 0; i < Size; i++)
                price += _elements[i].Price * (genom.GetChromosome()[i] ? 1 : 0);
            return price;
        }

        public int GetWeight(Genom genom)
        {
            int weight = 0;
            for (uint i = 0; i < Size; i++)
                weight += _elements[i].Weight * (genom.GetChromosome()[i] ? 1 : 0);
            return weight;
        }

        public double GetFitness(Genom elements)
        {
            return elements.Price * (elements.Weight > MaxWeight ? 0.2 : 1.0) / _totalPrice;
        }
    }
}
