﻿using System;
using System.Linq;

namespace task3_num2b_backpack.src.Genetic
{
    internal class Population
    {
        // размер популяции
        public uint Size { get; }
        // множество геномов данной популяции
        public Genom[] Individuals { get; private set; }
        // количество генов каждого генома
        private readonly uint _chromosomeLength;
        private double _totalFitness;

        public Population(uint populationSize, uint chromosomeLength)
        {
            Size = populationSize;
            _chromosomeLength = chromosomeLength;
            Individuals = new Genom[Size];
        }

        public Population(Population population)
        {
            Size = population.Size;
            _chromosomeLength = population._chromosomeLength;
            _totalFitness = population._totalFitness;
            Individuals = population.Individuals;
        }

        public void CalculatePopulationFitness()
        {
            _totalFitness = Individuals.Sum(i => i.Fitness);
        }

        // начало генерации популяции
        public void BeginPopulation()
        {
            var rnd = new Random();
            var items = new Backpack();

            for (uint i = 0; i < Size; i++)
            {
                var newChromosome = new bool[_chromosomeLength];
                for (uint j = 0; j < _chromosomeLength; j++)
                    newChromosome[j] = rnd.Next() % 2 == 0;

                Individuals[i] = new Genom(newChromosome);

                Individuals[i].Price = items.GetPrice(Individuals[i]);
                Individuals[i].Weight = items.GetWeight(Individuals[i]);
                Individuals[i].Fitness = items.GetFitness(Individuals[i]);
            }
            _totalFitness = Individuals.Sum(i => i.Fitness);
        }

        public void SetGenom(Genom genom, uint position)
        {
            Individuals[position] = genom;
        }

        // сортировка геномов популяции по порядку их значений фитнеса
        public void SortByFitness()
        {
            Individuals = Individuals.OrderBy(i => i.Fitness).ToArray();
        }

        // получаем наиболее приспособленную особь (геномы предварительно должны быть отсортированы)
        public Genom GetFittest()
        {
            return Individuals[Individuals.Length - 1];
        }

        // селекция методом рулетки
        public Genom RouletteSelection(out int num, int firstNum = -1)
        {
            var rnd = new Random();

            // задаем пороговое значение
            var threshold = rnd.NextDouble() * _totalFitness;

            double sum = 0;
            for (int i = 0; i < Size; i++)
            {
                num = i;
                sum += Individuals[i].Fitness;
                if (sum >= threshold && (firstNum == -1 || firstNum != i))
                    return Individuals[i];
            }
            num = (int)Size - 1;
            return Individuals.Last();
        }
    }
}
