﻿using System;

namespace task3_num2b_backpack.src.Genetic
{
    // основные операции генетического алгоритма
    internal static class GeneticOperators
    {
        // кроссинговер
        public static Tuple<Genom, Genom> UniformCrossover(bool[] first, bool[] second)
        {
            var rnd = new Random();

            if (rnd.NextDouble() < 0.2)
                return Tuple.Create(new Genom(first), new Genom(second));

            var firstChromosome = new bool[first.Length];
            var secondChromosome = new bool[first.Length];

            for (uint i = 0; i < first.Length; i++)
            {
                if (rnd.Next() % 2 == 0)
                {
                    firstChromosome[i] = first[i];
                    secondChromosome[i] = second[i];
                }
                else
                {
                    firstChromosome[i] = second[i];
                    secondChromosome[i] = first[i];
                }
            }
            return Tuple.Create(new Genom(firstChromosome), new Genom(secondChromosome));
        }

        // мутация для заданного генома
        public static void Mutation(ref Genom individual)
        {
            var rnd = new Random();
            for (uint i = 0; i < individual.ChromosomeLength; i++)
                // вероятность мутации каждого гена = 5%
                if (rnd.NextDouble() > 0.95)
                    individual.SetGen(!individual.GetGen(i), i);
        }
    }
}
