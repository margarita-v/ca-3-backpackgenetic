﻿namespace task3_num2b_backpack.src.Genetic
{
    // Класс представляет собой геном - возможный набор рюкзака
    internal class Genom
    {
        // размер хромосомы генома
        public int ChromosomeLength { get; private set; }
        // набор генов хромосомы; ген[i] = true, если вещь под номером i лежит в рюкзаке
        private readonly bool[] _chromosome;

        public double Fitness { get; set; }
        public double Price { get; set; }
        public double Weight { get; set; }

        public Genom() { }

        public Genom(bool[] chromosome)
        {
            _chromosome = chromosome;
            ChromosomeLength = chromosome.Length;
        }

        public bool[] GetChromosome() { return _chromosome; }

        public void SetGen(bool value, uint position)
        {
            _chromosome[position] = value;
        }

        public bool GetGen(uint position)
        {
            return _chromosome[position];
        }
    }
}
