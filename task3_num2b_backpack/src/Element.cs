﻿namespace task3_num2b_backpack.src
{
    // рюкзак
    public class Element
    {
        // вес предметов
        public int Weight { get; private set; }
        // цена предметов
        public int Price { get; private set; }

        public Element(int weight = 0, int price = 0)
        {
            Weight = weight;
            Price = price;
        }
    }
}
