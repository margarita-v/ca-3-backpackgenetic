﻿using System.Collections.Generic;
using System.Windows.Forms;
using task3_num2b_backpack.src.Genetic;

namespace task3_num2b_backpack.src
{
    public class Algorithm
    {
        // все предметы, которые можно сложить в рюкзак
        private readonly List<Element> _initialSet;
        // общее количество предметов, из которых будем выбирать подходящие
        private readonly int _size;

        // максимальный вес рюкзака
        private readonly int _maxWeight;
        // максимальная стоимость предметов
        private int _maxPrice;

        // номера тех предметов, которые входят в решение
        private readonly int[] _solve;
        // номера предметов, входящих в текущее решение
        private readonly int[] _currentSolve;
        
        public Algorithm(int maxWeight, List<Element> initialSet)
        {
            _initialSet = initialSet;
            _size = _initialSet.Count;
            _maxWeight = maxWeight;
            _solve = new int[_size];
            _currentSolve = new int[_size];
        }

        /// <summary>
        /// Решение задачи перебором вариантов
        /// </summary>
        /// <param name="maxWeight">Вес, который следует набрать из оставшихся предметов.</param>
        /// <param name="number">Порядковый номер группы предметов.</param>
        /// <param name="price">Стоимость текущего решения.</param>
        private void _getSolve(int maxWeight, int number, int price)
        {
            if (number >= _size && price > _maxPrice && maxWeight == 0)
            {
                for (int i = 0; i < _size; i++)
                    _solve[i] = _currentSolve[i];
                _maxPrice = price;
            }
            else if (number < _size)
            {
                for (int i = 0; i <= maxWeight/_initialSet[number].Weight; ++i)
                {
                    _currentSolve[number] = i;
                    var tmp = maxWeight - i*_initialSet[number].Weight;
                    if (tmp >= 0) 
                        _getSolve(tmp, number + 1, price + i*_initialSet[number].Price);
                }
            }
        }

        // решение задачи о рюкзаке методом простого перебора
        public void GetSolve(out int[] result)
        {
            _getSolve(_maxWeight, 0, 0);
            result = _solve;
        }

        // решение задачи о рюкзаке генетическим алгоритмом
        public void GetGeneticSolve(out int[] result)
        {
            var backpack = new Backpack(_initialSet, _maxWeight);

            // создаем первую популяцию
            var currentPopulation = new Population(Backpack.Size * 4, Backpack.Size);
            currentPopulation.BeginPopulation();
            currentPopulation.SortByFitness();

            for (uint i = 0; i < 200; i++)
            {
                // создаем новую популяцию
                var nextPopulation = new Population(Backpack.Size * 4, Backpack.Size);

                for (uint j = 0; j < nextPopulation.Size; j += 2)
                {
                    // выбираем родителей
                    int firstNum, secondNum;
                    var firstParent = currentPopulation.RouletteSelection(out firstNum);
                    var secondParent = currentPopulation.RouletteSelection(out secondNum, firstNum);

                    // скрещиваем родителей
                    var offsprings = GeneticOperators.UniformCrossover(firstParent.GetChromosome(), 
                        secondParent.GetChromosome());
                    var firstChild = offsprings.Item1;
                    var secondChild = offsprings.Item2;

                    // вводим мутации каждому из потомков, полученных в результате скрещивания
                    GeneticOperators.Mutation(ref firstChild);
                    GeneticOperators.Mutation(ref secondChild);

                    firstChild.Price = backpack.GetPrice(firstChild);
                    firstChild.Weight = backpack.GetWeight(firstChild);
                    firstChild.Fitness = backpack.GetFitness(firstChild);

                    secondChild.Price = backpack.GetPrice(secondChild);
                    secondChild.Weight = backpack.GetWeight(secondChild);
                    secondChild.Fitness = backpack.GetFitness(secondChild);

                    // сохраняем полученных потомков в следующем поколении
                    nextPopulation.SetGenom(firstChild, j);
                    nextPopulation.SetGenom(secondChild, j + 1);
                }
                nextPopulation.SortByFitness();
                nextPopulation.SetGenom(currentPopulation.GetFittest(), 0);
                nextPopulation.SortByFitness();
                nextPopulation.CalculatePopulationFitness();

                currentPopulation = new Population(nextPopulation);
            } // for
            var res = currentPopulation.GetFittest();
            result = new int[_size];
            for (int i = 0; i < _size; i++)
                if (res.GetChromosome()[i])
                    result[i]++;
        }

        /*private void _try(int index, int weight, int price)
        {
            if (_initialBackpack.Weight[index] <= weight)
            {
                _currentSolve[index]++;
                price += _initialBackpack.Price[index];
                weight -= _initialBackpack.Weight[index];

                if (index < _size - 1)
                    _try(index + 1, weight, price);
                else if (price > _maxPrice)
                {
                    for (int i = 0; i < _size; i++)
                        _solve[i] = _currentSolve[i];
                    _maxPrice = price;
                    _maxWeightResult = weight;
                }

                weight += _initialBackpack.Weight[index];
                price -= _initialBackpack.Price[index];
                _currentSolve[index]--;
            }

            if (index < _size - 1)
                _try(index + 1, weight, price);
            else if (price > _maxPrice)
            {
                _maxPrice = price;
                _maxWeightResult = weight;
                for (int i = 0; i < _size; i++)
                    _solve[i] = _currentSolve[i];
            }
        }*/
    }
}
